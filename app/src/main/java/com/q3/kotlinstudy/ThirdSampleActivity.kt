package com.q3.kotlinstudy

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.solver.widgets.Rectangle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_third_sample.*

//https://kotlinlang.org/docs/reference/idioms.html


class ThirdSampleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third_sample)
        // filterCollection()
        // tvData.text = addThreeNumbers(5,4).toString()
        // anotherFilterExample()
        //mapSample()
        //forUntilLoopSample()
        //lazySample()
        //notNullTest()
        //tryCatchSample("9l,k")
        var res = arrayOfMinusOnes(5)
        res.forEach { tvData.text = tvData.text.toString() + "\n$it" }
    }

    fun foo(): Int {     // bad
        return 1
    }

    fun foo1() = 1        // good

    /**
     * Collection Filter Example
     */
    fun filterCollection() {
        var fruits = listOf<String>("Mango", "Lichi", "Apple", "Banana", "Guava", "Cucumber")
        fruits.filter { it.startsWith("A") }
                .sortedBy { it }
                .map { it.toUpperCase() }
                .forEach { tvData.text = tvData.text.toString()   +" $it" }
    }

    /**
     * Collection Filter Example
     */
    fun anotherFilterExample(){
        val numbers = listOf<Int>(4,-5,9,-7,8,52,-26,0,-78,68)
        //val positiveNumbers = numbers.filter { it > 0 }
        val positiveNumbers = numbers.filter { x -> x > 0 }
        positiveNumbers.forEach { tvData.text = tvData.text.toString()   +" $it" }
        val negativeNumbers = numbers.filter { it < 0 }
    }

    /**
     * Create object of class
     */
    fun createClassObjects(){
        var rectangle = Rectangle()
        rectangle.height = 10
        rectangle.width = 25
        var mainActivity = MainActivity()
    }

    /**
     * Default Value in function parameter
     */
    fun addThreeNumbers(a: Int, b: Int = 6, c:Int = 4): Int{
        return  a + b + c
    }

    /**
     * Instance check using When
     */
    fun checkInstances(obj: Any): String =
            when(obj){
                obj is Rectangle -> "Object of Rectangle Class"
                obj is MainActivity -> "Object of Main Activity"
                else -> "No Match Found"
            }

    /**
     * Map Example
     */
    fun mapSample(){

        var mapData = mapOf<String, Int>("a" to 1, "b" to 2, "c" to 3)
        var mapData2 = mapOf<Int, String>(1 to "One", 2 to "Two", 3 to "Three")

        for ((k,v) in mapData){
            tvData.text = tvData.text.toString() + "\nKey=$k  Value=$v"
        }
        tvData.text = tvData.text.toString() + "\n${mapData["b"]}\n"

        for ((k,v) in mapData2){
            tvData.text = tvData.text.toString() + "\nKey=$k  Value=$v"
        }
        tvData.text = tvData.text.toString() + "\n\n\n"

        mapData.forEach { tvData.text = tvData.text.toString() + "\nKey=${it.key}  Value=${it.value}" }
    }

    /**
     * Until For Loop Example
     */
    fun forUntilLoopSample(){
        for (i in 1..5){
            tvData.text = tvData.text.toString() + "\n $i"
        }

        tvData.text = tvData.text.toString() + "\n\n\n"

        for (i in 1 until 5){
            tvData.text = tvData.text.toString() + "\n $i"
        }
    }

    /**
     * Method on Lazy property
     */
    fun lazySample(){
        val s:String by lazy {
            var demo = "Apple" + " Mango" + " Banana"
            demo
        }
        tvData.text = tvData.text.toString() + "\n\n\n$s"
        tvData.text = tvData.text.toString() + "\n\n\n${s.spaceToCamelCase()}"

    }

    /**
     * Extension Functions
     */
    fun String.spaceToCamelCase(): String{
        return "APPLE"
    }

    /**
     * Not Null check
     */
    fun notNullTest(){
        var strArray = listOf("a","b")
        var count = strArray?.size ?: 0
        count?.let {
            Toast.makeText(this, "NOT NULL", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Return result from try catch body
     */
    fun tryCatchSample(str: String){
        val x: Int
        x = try {
            str.toInt()
        } catch (e: Exception) {
             0
        }
        tvData.text = x.toString()
    }

    fun ifElseResultSample(a: Int, b: Int){
        var res: Int
        res = if (a > b){
            a
        } else {
            b
        }
        res = if (a > b) a else b
    }

    //Builder Function
    fun arrayOfMinusOnes(size: Int): IntArray {
        return IntArray(size).apply { fill(-1) }
    }

    /**
     * Single line function
     */
    fun theAnswer() = 42

    /**
     * Equivalent to
     */
    fun theAnswer2(): Int {
        return 42
    }


}
