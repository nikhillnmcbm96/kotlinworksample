package com.q3.kotlinstudy

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class FourthSampleDataType : AppCompatActivity() {

    //You can use underscores to make number constants more readable:
    val oneMillion = 1_000_000
    val creditCardNumber = 1234_5678_9012_3456L
    val socialSecurityNumber = 999_99_9999L
    val hexBytes = 0xFF_EC_DE_5E
    val bytes = 0b11010010_01101001_10010100_10010010

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fourth_sample_data_type)
        //useOfBreakContinue()
        foo()
        /*   Type	Bit width
           Double	64
           Float	32
           Long	64
           Int	    32
           Short	16
           Byte	8

           Longs are tagged by a capital L: 123L
   Hexadecimals: 0x0F
   Binaries: 0b00001011

   Doubles by default: 123.5, 123.5e10

   Floats are tagged by f or F: 123.5f


   Here is the complete list of bitwise operations (available for Int and Long only):

   shl(bits) – signed shift left (Java's <<)
   shr(bits) – signed shift right (Java's >>)
   ushr(bits) – unsigned shift right (Java's >>>)
   and(bits) – bitwise and
   or(bits) – bitwise or
   xor(bits) – bitwise xor
   inv() – bitwise inversion


   Default Imports
   A number of packages are imported into every Kotlin file by default:

   kotlin.*
   kotlin.annotation.*
   kotlin.collections.*
   kotlin.comparisons.* (since 1.1)
   kotlin.io.*
   kotlin.ranges.*
   kotlin.sequences.*
   kotlin.text.*
           */
    }

    fun kotlinSample(){
        // Creates an Array<String> with values ["0", "1", "4", "9", "16"]
        val asc = Array(5, { i -> (i * i).toString() })
        asc.forEach { println(it) }


        val x: IntArray = intArrayOf(1, 2, 3)
        x[0] = x[1] + x[2]
    }

    fun stringSample(){

        val text = """
            for (c in "foo")print(c)
            """

        println(text)
    }

    fun useOfBreakContinue(){
       /* for (i in 1..10){
            println(i)
            if (i == 5) break
        }

        for (i in 1..10){
            if (i %2 == 0) continue
            println(i)
        }*/

        loop@ for (i in 1..10) {
            for (j in 1..10) {
                println(i)
                if (j == 2) break@loop
            }
        }
    }

    fun foo() {
        listOf(1, 2, 3, 4, 5).forEach lit@{
            if (it == 3) return@lit // local return to the caller of the lambda, i.e. the forEach loop
            print(it)
        }
        print(" done with explicit label")
    }
}
