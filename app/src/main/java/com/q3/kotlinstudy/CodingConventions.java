package com.q3.kotlinstudy;

import android.webkit.JavascriptInterface;

public class CodingConventions {

    /*

    Same as Java

Colon
Put a space before : in the following cases:

when it's used to separate a type and a supertype;
when delegating to a superclass constructor or a different constructor of the same class;
after the object keyword.
Don't put a space before : when it separates a declaration and its type.

Always put a space after :.


abstract class Foo<out T : Any> : IFoo {
    abstract fun foo(a: Int): T
}
​
class FooImpl : Foo() {
    constructor(x: String) : this(x) { ... }

    val x = object : IFoo { ... }
}
     */
}
