package com.q3.kotlinstudy

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

/*
    Using Kotlin for Android Development

    Compatibility : JDK6
    Performance
    Interoperability :, allowing to use all existing Android libraries
    Compilation Time
    Learning Curve: : For a Java developer, getting started with Kotlin is very easy. The automated Java to Kotlin converter
    included in the Kotlin plugin helps with the first steps

    Some Quick Results:
    Kotlin has been successfully adopted by major companies, and a few of them have shared their experiences:
    Pinterest has successfully introduced Kotlin into their application, used by 150M people every month.
    Basecamp's Android app is 100% Kotlin code, and they report a huge difference in programmer happiness and great improvements in work quality and speed.
    Keepsafe's App Lock app has also been converted to 100% Kotlin, leading to a 30% decrease in source line count and 10% decrease in method count
*/


/*
 Discussions on Val and Var
 Functions / Methods
 Commenting
 $ operator
 */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvResult.text = addTwoNumbers(9,5).toString()
        tvResult2.text = "" + subtractNumbers(9,5)
        printCompanyName()
        printPlatformName()
        workOnDollar()

        btnNext.setOnClickListener{
            val intent = Intent(this, ConditionalAndLoopActivity::class.java)
            startActivity(intent)
        }
    }

    fun addTwoNumbers(x: Int, y: Int):Int{
        //We can ignore these two lines into using single line return x + y
        var res = x + y
        return res
    }

    fun subtractNumbers( x : Int, y: Int) = x - y

    fun printCompanyName(): Unit{
        tvResult.text = tvResult.text.toString() +  " Q3 Technologies"
    }

    fun printPlatformName(){
        tvResult.text = tvResult.text.toString() +  " Android"
    }

    fun workOnDollar(){
        var a = 10
        val str = "a is $a"
        a = 20
        //val str2 = "${str.replace("is", "was")} but now $a"
        val str2 = "${str.replace("is","was")} but now $a"
        tvTest.text = str2
    }
}

