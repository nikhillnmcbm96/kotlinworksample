package com.q3.kotlinstudy.classobjectsample

open class Student  constructor(name: String, roll: String, age: Int) {
    // constructor(optional) keyword for primary constructor use only if access specifier or notations required
    var name: String
    var roll: String
    var age:Int
    var marks: Int = 0
    open var collegeName: String  = "Q3 Classes";

    init {
        this.name = name
        this.roll = roll
        this.age = age
    }

    init {
        println("This is the second initializer")
    }

    init {
        println("This is the third initializer")
    }

    constructor(name: String, roll: String, age: Int, marks: Int) : this(name, roll, age) {
        this.marks = marks
    }

    //Note: The sequence of execution is always like Primary Constructor -> Init -> Secondary Constructor
    //Note: Every class has a default empty constructor that called if we do not define any constuctor
    //Note: By default constructors use public access specifier
    //Note: Super class of every class is Any and it has toString, hashcode and equals method
    //Note: By default all classes are final and to make it open must use open keyword before class keyword.

    /*    Members of class are
    1. Constructer and Initialization
    2. Functions
    3. Properties
    4. Nested / Inner Classes
    5. Object Declaration*/

    open fun printStudentName(){
        println("Base Student class : " + this.name)
    }
}