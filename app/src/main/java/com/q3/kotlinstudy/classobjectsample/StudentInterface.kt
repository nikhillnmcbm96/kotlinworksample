package com.q3.kotlinstudy.classobjectsample

interface StudentInterface {
    var name: String
    fun printStudentName(){
        println(" Student Interface Length: " + this.name.length)
    }
}