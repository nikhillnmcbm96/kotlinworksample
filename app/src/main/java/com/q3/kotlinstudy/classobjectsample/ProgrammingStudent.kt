package com.q3.kotlinstudy.classobjectsample

class ProgrammingStudent(name: String, roll: String, age: Int, knownProLang: String, expertInLang: String, collegeName: String) : Student(name, roll, age) {

    override var collegeName: String = "j";

    init {
        println("Programming Language Student instance created.")
        this.collegeName = collegeName
    }

    inner class TestInnerClass{
        constructor(){
            println("From Inner Class : " + super@ProgrammingStudent.collegeName)
        }
    }

    override fun printStudentName(){
        println("Derived ProgrammingStudent class : " + this.name)
    }

    fun testData(){
        println(collegeName +" : College Name from Derive Class Programming Student : Then " + super.collegeName + " From SUPER : ")
        printStudentName()
        super.printStudentName()
    }


    //Note: By default override method always exist in open
    //Note: We can override val to var but not vice versa.
    //Inheritance process: base class constructor -> Base class init -> Derive class constructor -> Derive class init
    //Avoid use of open members in constructor or init of base class
}