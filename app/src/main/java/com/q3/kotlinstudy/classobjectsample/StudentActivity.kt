package com.q3.kotlinstudy.classobjectsample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.q3.kotlinstudy.R
import kotlinx.android.synthetic.main.activity_student.*

class StudentActivity : AppCompatActivity() {

    lateinit var teacher:Teacher

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student)
       // instantiateStudent()
       // CommonUtils.printToast(this@StudentActivity, "Static Method Testing" + CommonUtils.URL);
       // instanceProgrammingStudent()
        instanceManagementStudent()
    }

    private fun instanceManagementStudent() {
       var  managementStudent = ManagementStudent("Anurag Pal", "123456", 26, "HR" )
        managementStudent.printStudentName()
    }

    private fun instanceProgrammingStudent() {
        var programmingStudent = ProgrammingStudent("Anurag Pal", "123456", 26, "C,C++,Java,Kotlin", "Java","BTI College")
        programmingStudent.testData()
        programmingStudent.TestInnerClass()
    }

    private fun instantiateStudent() {
        var student1 = Student("Nikhil Kr", "095096", 26)
        var student2 = Student("Abhishek Kr", "095023", 28, 99)
        tvStudentDetail1.text = student1.name + "\n" + student1.marks
        tvStudentDetail2.text = student2.name + "\n" + student2.marks
        student1.printStudentName()
    }



}
