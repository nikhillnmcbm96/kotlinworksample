package com.q3.kotlinstudy.classobjectsample

class ManagementStudent : Student , StudentInterface {
    constructor(name: String, roll: String, age: Int, managementPaper: String) : super(name, roll, age){
        println("ManagementStudent constructor.")
    }

    init {
        println("Management Student instance created.")
    }

    final override fun printStudentName(){
        println("Derived ProgrammingStudent class : " + this.name)
        super<StudentInterface>.printStudentName()
        super<Student>.printStudentName()
    }

    //Note: By default override method always exist in open to restrict this we use final keyword
}