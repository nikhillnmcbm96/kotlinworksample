package com.q3.kotlinstudy.classobjectsample

class Teacher(var teacherName: String, var teacherFaculty: String) {
    fun printTeacherName(){
        println("Base Teacher class : " + this.teacherName)
    }
}