package com.q3.kotlinstudy.classobjectsample

import android.content.Context
import android.util.Log
import android.widget.Toast

const val DATA: String = "Const Test"

class CommonUtils {

    companion object {
        val URL: String = "www.q3tech.com"

        fun printToast(context: Context, message: String){
            Toast.makeText(context, message +" $DATA" , Toast.LENGTH_SHORT).show()
        }

        fun logE(context: Context, message: String){
            Log.e("DATA : ", message)
        }
    }
}