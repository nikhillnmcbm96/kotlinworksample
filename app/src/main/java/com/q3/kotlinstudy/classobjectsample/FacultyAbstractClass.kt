package com.q3.kotlinstudy.classobjectsample

abstract class FacultyAbstractClass {
    abstract fun printKotlinOwner()

    fun printClassName(){
        println("FacultyAbstractClass.kotlin")
    }
}