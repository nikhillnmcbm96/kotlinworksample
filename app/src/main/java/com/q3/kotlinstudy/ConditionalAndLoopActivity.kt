package com.q3.kotlinstudy

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.q3.kotlinstudy.R.id.tvResult3
import kotlinx.android.synthetic.main.activity_conditional_and_loop.*
import kotlinx.android.synthetic.main.activity_conditional_and_loop.*
import java.util.*

class ConditionalAndLoopActivity : AppCompatActivity() {

    /**
     * If statement
     * When Statement
     * ? operator
     * for
     * while
     * step
     * In and !in
     * parse data
     * .. operater
     * down to
     */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_conditional_and_loop)
        tvResult1.text = largestFromTwo(5,3).toString()
        tvResult2.text = largestFromTwoAnother(4,8).toString()
        tvResult1.text = tvResult1.text.toString() + "    " + largestNumber(12, 16).toString()

        //  tvResult3.text = getMultiplyResult("ram","5").toString()

       /* var result: String ;
        result = whenSample(5)
        result = result + "\n${whenSample("Hi")}"
        result = result + "\n${whenSample(1.5)}"
        result = result + "\n${whenSample(true)}"
        result = result + "\n${whenSample("Nothing")}"*/
        forLoopWithIn()
       // tvResult3.setText(result)

        //forLoopWithIn()

        //loopWithSteps()
        //forLoopSample1()
       // anotherInterestingWhenExample()
    }

    fun largestFromTwo(x: Int, y:Int): Int{
        if(x > y){
            return x
        }
        else{
            return y
        }
    }

    fun largestNumber(x : Int, y : Int) = if (x > y) x else y

    fun largestFromTwoAnother(x: Int, y:Int) : Int?{
        when (x>y){
            true ->  return null
            false -> return y
        }
    }

    fun largestFromTwoAnother2(x: Int, y:Int) : Int=
            when (x>y){ true ->   x + y false ->  y }

    fun getLengthOfString(str : Any): Int?{
        if (str is String) return str.length
        else return null
    }

    fun getStringLength(obj: Any): Int? {
       if (obj is String && obj.length > 0) {
            return obj.length
        }
        return null
    }

    fun getMultiplyResult(x: String, y: String): Int{
        var data1: Int?
        try {
            data1 = x.toInt()
        } catch (e: NumberFormatException) {
            data1 = null
        }
        var data2: Int?
        try {
            data2 = y.toInt()
        } catch (e: NumberFormatException) {
            data2 = null
        }

        if (data1!= null && data2!=null) {
            return data1 * data2
        }else{
            return 0
        }
    }

    fun forLoopSample1(){
        val array = listOf("Apple", "Mango", "Banana", "Graps")/*
        for (item in array) {
            tvResult3.text = tvResult3.text.toString() + " " + item
        }*/

        for (index in array.indices){
            //tvResult3.text = tvResult3.text.toString() + " $index"
            tvResult3.text = tvResult3.text.toString() + " ${array[index]}"
        }
    }


    fun whileLoopSample(){
        val array = listOf("Nikhil", "Anurag", "Abhishek", "Rajesh", "Himanshu", "Kausal")
        var index = 0
        while (index < array.size){
            tvResult3.text = tvResult3.text.toString() + " ${array[index]}"
            index++
        }
    }

    fun whenSample(data: Any): String =
        when(data){
            1 -> "One"
            1.5 -> "One point Five"
            "Hi" -> "Hello"
            true -> "TRUE Boolean"
            else -> "Unmatched Value"
        }

    fun forLoopWithIn(){
        var de = 10
        for (i in 1..de){
            tvResult3.setText(tvResult3.text.toString() + "\n$i")
           // tvResult3.text = tvResult3.text.toString() + "\n$i"
        }

        var x = 5
        if (x in 1..10 step 3){
            Toast.makeText(this, "TRUE",Toast.LENGTH_SHORT).show()
        }

        if (x !in 50..100){
            Toast.makeText(this, "NOT IN RANGE",Toast.LENGTH_SHORT).show()
        }
    }

    fun loopWithSteps(){
       /* for (i in 1..20 step 2){
            tvResult3.setText(tvResult3.text.toString() + "\n$i")
        }*/

        for (i in 20 downTo 1 step 2){
            tvResult3.setText(tvResult3.text.toString() + "\n$i")
        }

      /*  Caused by: java.lang.IllegalArgumentException: Step must be positive, was: -2. */
    }

    fun anotherInterestingWhenExample(){
        val array = listOf("Nikhil", "Anurag", "Abhishek", "Rajesh", "Himanshu", "Kausal")
        when{
            "Anurag" in array -> tvResult3.text = "Anurag there"
            "Nikhil" in array -> tvResult3.text = "Nikhil there"
            "Kavita" !in  array -> tvResult3.text = "Kavita not there"
        }
    }

}
