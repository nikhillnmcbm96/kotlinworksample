package com.q3.kotlinstudy.dataclass

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.q3.kotlinstudy.R
import kotlinx.android.synthetic.main.activity_data_class_sample.*

class DataClassSample : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_class_sample)

        var userData1 = UserData("nikhil","123456");
        var userData2 = UserData("nikhil","123456");
        var  userData3 = userData1.copy()
        var user1 = User("anurag","789741");
        val (userName, password) = userData1
       // val (userName1, password2) = user1
        println(userData1)
        println(user1)
        println(userData3)
        var user2 = User("anurag","789741");
        tvUserName.setText(userData1.equals(userData2).toString())
        tvUserPass.setText(user1.equals(user2).toString())
    }
}
